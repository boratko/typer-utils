# Typer Utils

Utilities modifying / extending [Typer](https://typer.tiangolo.com/).

## Current Features

- Do not convert option names (for compatibility with libraries requiring option names to be valid variable names, eg. [wandb](https://wandb.ai/))
- Decorator to unwrap defaults from `typer.Argument` and `typer.Option` so functions can still be used normally

## Instructions

```python
import typer
from typer_utils import no_option_conversion, unwrap_typer_param

app = typer.Typer()

no_option_conversion()


@app.command()
@unwrap_typer_param
def func(arg: str = typer.Argument(...), Some_Option: int = typer.Option(2)):
    pass
```

This will create a command with option `--Some_Option`, due to the `no_option_conversion()` call. The function `func` will also be usable as a normal function, with appropriate defaults, due to `@unwrap_typer_param`.