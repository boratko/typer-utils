import pytest
from typer_utils import unwrap_typer_param
import typer
from typer.testing import CliRunner

runner = CliRunner()


nodefault = {
    "nodefault": "",
    "arg_nodefault": "=typer.Argument(...)",
    "opt_nodefault": "=typer.Option(...)",
}

default = {
    "default": '="default"',
    "arg_default": '=typer.Argument("default")',
    "opt_default": '=typer.Option("default")',
}

param_values = {
    **nodefault,
    **default,
}


def create_function(*values):
    param_names = [f"param_{i}" for i in range(len(values))]
    params = [f"{param_name}{value}" for param_name, value in zip(param_names, values)]
    function_string = f"def f({', '.join(params)}): return {', '.join(param_names)}"
    try:
        exec(function_string, globals())
    except SyntaxError as e:
        pytest.skip(e.msg)
    return f


@pytest.mark.parametrize("param_0", nodefault)
def test_func_one_param_no_default(param_0):
    f = unwrap_typer_param(create_function(param_values[param_0]))
    assert f(0) == 0
    assert f(param_0=0) == 0
    with pytest.raises(TypeError):
        assert f()


@pytest.mark.parametrize("param_0", default)
def test_func_one_param_default(param_0):
    f = unwrap_typer_param(create_function(param_values[param_0]))
    assert f(0) == 0
    assert f(param_0=0) == 0
    assert f() == "default"


@pytest.mark.parametrize("param_1", nodefault)
@pytest.mark.parametrize("param_0", nodefault)
def test_func_two_param_nodefault(param_0, param_1):
    f = unwrap_typer_param(
        create_function(param_values[param_0], param_values[param_1])
    )
    with pytest.raises(TypeError):
        assert f() == ("default", "default")
    with pytest.raises(TypeError):
        assert f(0) == (0, "default")
    assert f(0, 1) == (0, 1)
    assert f(param_0=0, param_1=1) == (0, 1)
    with pytest.raises(TypeError):
        assert f(param_1=1) == ("default", 1)
    with pytest.raises(TypeError):
        assert f(param_0=0) == (0, "default")


@pytest.mark.parametrize("param_1", default)
@pytest.mark.parametrize("param_0", default)
def test_func_two_param_default(param_0, param_1):
    f = unwrap_typer_param(
        create_function(param_values[param_0], param_values[param_1])
    )
    assert f() == ("default", "default")
    assert f(0) == (0, "default")
    assert f(0, 1) == (0, 1)
    assert f(param_0=0, param_1=1) == (0, 1)
    assert f(param_1=1) == ("default", 1)
    assert f(param_0=0) == (0, "default")


@pytest.mark.parametrize("param_1", default)
@pytest.mark.parametrize("param_0", nodefault)
def test_func_two_param_nodefault_default(param_0, param_1):
    f = unwrap_typer_param(
        create_function(param_values[param_0], param_values[param_1])
    )
    with pytest.raises(TypeError):
        assert f() == ("default", "default")
    assert f(0) == (0, "default")
    assert f(0, 1) == (0, 1)
    assert f(param_0=0, param_1=1) == (0, 1)
    with pytest.raises(TypeError):
        assert f(param_1=1) == ("default", 1)
    assert f(param_0=0) == (0, "default")


@pytest.mark.parametrize("param_1", nodefault)
@pytest.mark.parametrize("param_0", default)
def test_func_two_param_default_nodefault(param_0, param_1):
    f = create_function(param_values[param_0], param_values[param_1])
    with pytest.raises(SyntaxError):
        unwrap_typer_param(f)


@pytest.mark.parametrize("param_2", default)
@pytest.mark.parametrize("param_1", default)
@pytest.mark.parametrize("param_0", default)
def test_func_three_param_default(param_0, param_1, param_2):
    f = unwrap_typer_param(
        create_function(
            param_values[param_0], param_values[param_1], param_values[param_2]
        )
    )
    assert f() == ("default", "default", "default")
    assert f(0) == (0, "default", "default")
    assert f(0, 1) == (0, 1, "default")
    assert f(0, 1, 2) == (0, 1, 2)
    assert f(param_0=0, param_1=1, param_2=2) == (0, 1, 2)
    assert f(param_1=1, param_2=2) == ("default", 1, 2)
    assert f(param_0=0, param_2=2) == (0, "default", 2)
    assert f(param_0=0, param_1=1) == (0, 1, "default")
    assert f(param_0=0) == (0, "default", "default")
    assert f(param_1=1) == ("default", 1, "default")
    assert f(param_2=2) == ("default", "default", 2)


@pytest.mark.parametrize("param_2", default)
@pytest.mark.parametrize("param_1", default)
@pytest.mark.parametrize("param_0", nodefault)
def test_func_three_param_nodefault_default_default(param_0, param_1, param_2):
    f = unwrap_typer_param(
        create_function(
            param_values[param_0], param_values[param_1], param_values[param_2]
        )
    )
    with pytest.raises(TypeError):
        assert f() == ("default", "default", "default")
    assert f(0) == (0, "default", "default")
    assert f(0, 1) == (0, 1, "default")
    assert f(0, 1, 2) == (0, 1, 2)
    assert f(param_0=0, param_1=1, param_2=2) == (0, 1, 2)
    with pytest.raises(TypeError):
        assert f(param_1=1, param_2=2) == ("default", 1, 2)
    assert f(param_0=0, param_2=2) == (0, "default", 2)
    assert f(param_0=0, param_1=1) == (0, 1, "default")
    assert f(param_0=0) == (0, "default", "default")
    with pytest.raises(TypeError):
        assert f(param_1=1) == ("default", 1, "default")
    with pytest.raises(TypeError):
        assert f(param_2=2) == ("default", "default", 2)


@pytest.mark.parametrize("param_2", default)
@pytest.mark.parametrize("param_1", nodefault)
@pytest.mark.parametrize("param_0", nodefault)
def test_func_three_param_nodefault_nodefault_default(param_0, param_1, param_2):
    f = unwrap_typer_param(
        create_function(
            param_values[param_0], param_values[param_1], param_values[param_2]
        )
    )
    with pytest.raises(TypeError):
        assert f() == ("default", "default", "default")
    with pytest.raises(TypeError):
        assert f(0) == (0, "default", "default")
    assert f(0, 1) == (0, 1, "default")
    assert f(0, 1, 2) == (0, 1, 2)
    assert f(param_0=0, param_1=1, param_2=2) == (0, 1, 2)
    with pytest.raises(TypeError):
        assert f(param_1=1, param_2=2) == ("default", 1, 2)
    with pytest.raises(TypeError):
        assert f(param_0=0, param_2=2) == (0, "default", 2)
    assert f(param_0=0, param_1=1) == (0, 1, "default")
    with pytest.raises(TypeError):
        assert f(param_0=0) == (0, "default", "default")
    with pytest.raises(TypeError):
        assert f(param_1=1) == ("default", 1, "default")
    with pytest.raises(TypeError):
        assert f(param_2=2) == ("default", "default", 2)


@pytest.mark.parametrize("param_2", nodefault)
@pytest.mark.parametrize("param_1", nodefault)
@pytest.mark.parametrize("param_0", nodefault)
def test_func_three_param_nodefault(param_0, param_1, param_2):
    f = unwrap_typer_param(
        create_function(
            param_values[param_0], param_values[param_1], param_values[param_2]
        )
    )
    with pytest.raises(TypeError):
        assert f() == ("default", "default", "default")
    with pytest.raises(TypeError):
        assert f(0) == (0, "default", "default")
    with pytest.raises(TypeError):
        assert f(0, 1) == (0, 1, "default")
    assert f(0, 1, 2) == (0, 1, 2)
    assert f(param_0=0, param_1=1, param_2=2) == (0, 1, 2)
    with pytest.raises(TypeError):
        assert f(param_1=1, param_2=2) == ("default", 1, 2)
    with pytest.raises(TypeError):
        assert f(param_0=0, param_2=2) == (0, "default", 2)
    with pytest.raises(TypeError):
        assert f(param_0=0, param_1=1) == (0, 1, "default")
    with pytest.raises(TypeError):
        assert f(param_0=0) == (0, "default", "default")
    with pytest.raises(TypeError):
        assert f(param_1=1) == ("default", 1, "default")
    with pytest.raises(TypeError):
        assert f(param_2=2) == ("default", "default", 2)


@pytest.mark.parametrize("param_2", nodefault)
@pytest.mark.parametrize("param_1", nodefault)
@pytest.mark.parametrize("param_0", default)
def test_func_three_param_default_nodefault_nodefault(param_0, param_1, param_2):
    f = create_function(
        param_values[param_0], param_values[param_1], param_values[param_2]
    )
    with pytest.raises(SyntaxError):
        unwrap_typer_param(f)


@pytest.mark.parametrize("param_2", nodefault)
@pytest.mark.parametrize("param_1", default)
@pytest.mark.parametrize("param_0", nodefault)
def test_func_three_param_nodefault_default_nodefault(param_0, param_1, param_2):
    f = create_function(
        param_values[param_0], param_values[param_1], param_values[param_2]
    )
    with pytest.raises(SyntaxError):
        unwrap_typer_param(f)


@pytest.mark.parametrize("param_2", nodefault)
@pytest.mark.parametrize("param_1", default)
@pytest.mark.parametrize("param_0", default)
def test_func_three_param_default_default_nodefault(param_0, param_1, param_2):
    f = create_function(
        param_values[param_0], param_values[param_1], param_values[param_2]
    )
    with pytest.raises(SyntaxError):
        unwrap_typer_param(f)


@pytest.mark.parametrize("param_2", default)
@pytest.mark.parametrize("param_1", nodefault)
@pytest.mark.parametrize("param_0", default)
def test_func_three_param_default_nodefault_default(param_0, param_1, param_2):
    f = create_function(
        param_values[param_0], param_values[param_1], param_values[param_2]
    )
    with pytest.raises(SyntaxError):
        unwrap_typer_param(f)


def test_func_still_works_with_typer():
    app = typer.Typer()

    @unwrap_typer_param
    @app.command()
    def f(
        param_0: int,
        param_1: int = typer.Argument(..., help="something"),
        param_2: int = typer.Option(2),
    ):
        return (param_0, param_1, param_2) == (0, 1, 2)

    result = runner.invoke(app, ["0", "1"])
    assert result.exit_code == 0

    assert f(0, 1)


def test_order_of_unwrap_does_not_matter():
    app = typer.Typer()

    @app.command()
    @unwrap_typer_param
    def f(
        param_0: int,
        param_1: int = typer.Argument(..., help="something"),
        param_2: int = typer.Option(2),
    ):
        return (param_0, param_1, param_2) == (0, 1, 2)

    result = runner.invoke(app, ["0", "1"])
    assert result.exit_code == 0

    assert f(0, 1)
