import typer
from typer.testing import CliRunner
from typer_utils import no_option_conversion

runner = CliRunner()


def test_no_underscore_to_hyphen():
    app = typer.Typer()

    no_option_conversion()

    @app.command()
    def f(Option_Name: int = typer.Option(...)):
        return Option_Name == 1

    result = runner.invoke(app, ["--Option_Name", "1"])
    assert result.exit_code == 0
